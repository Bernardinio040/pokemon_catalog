import React from "react";

import "./tile.css";

export default class Tile extends React.Component {
    constructor(props){
        super(props);
      }
      
      render(){
        let id = this.props.id.toString().padStart(3, '0');
        let path = './assets/images/' + id + `.png`;  

        return (
          <div className="tile">
              <img src={require(`${path}`)} />
              <h4>{this.props.name}</h4>
          </div>
        );
      }
}