import React from "react";

export default class ToogleButton extends React.Component {
    constructor(props){
        super(props);
    }

    sortingFunction = () => {
        let sortedPokemon = [];
        if (this.props.bttType.by == "name") {
            if (this.props.bttType.type == "asc") {
                sortedPokemon = this.props.pokedex.sort((a,b)=>{
                    return a.name.english > b.name.english ? 1 : a.name.english < b.name.english ? -1 : 0;
                    // return a.name.english.localeCompare(b.name.english);
                });
            } else if (this.props.bttType.type == "desc") {
                sortedPokemon = this.props.pokedex.sort((a,b)=>{
                    return a.name.english < b.name.english ? 1 : a.name.english > b.name.english ? -1 : 0;
                });
            }
        }

        if (this.props.bttType.by == "id") {
            if (this.props.bttType.type == "asc") {
                sortedPokemon = this.props.pokedex.sort((a,b)=>{
                    return a.id > b.id ? 1 : a.id < b.id ? -1 : 0;
                });
            } else if (this.props.bttType.type == "desc") {
                sortedPokemon = this.props.pokedex.sort((a,b)=>{
                    return a.id < b.id ? 1 : a.id > b.id ? -1 : 0;
                });
            }
        }
        
        this.props.dataUpdateCallback([...sortedPokemon], this.props.bttValue);
    }

    render(){
        return(
            <button onClick={ this.sortingFunction } style={{ backgroundColor: this.props.value == this.props.bttValue ? "green" : "#EEE", marginRight: "8px" }}> { this.props.bttValue } </button>
        );
    }
}