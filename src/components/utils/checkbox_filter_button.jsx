import React from "react";

export default class CheckboxFilterButton extends React.Component {
    constructor(props){
        super(props);
    }

    filteringFunction = () => {
        if (this.props.filters.includes(this.props.chbValue)) {
            let table = this.props.filters.filter(type => type != this.props.chbValue);
            this.props.dataUpdateCallback([...this.props.pokemons] , {}, [...table]);
        } else {
            //let filteredPokemon = [];
            let table = this.props.filters.concat(this.props.chbValue);
            // table.map((type) => {
            //    filteredPokemon.push(this.props.pokemons.filter(pokemon => pokemon.type == type));
            // })
            //console.log(filteredPokemon);
            this.props.dataUpdateCallback([...this.props.pokemons] , {}, [...table]);
        }
    }

    render() {
        return(
            <button onClick={ this.filteringFunction } style={{ backgroundColor: this.props.filters.includes(this.props.chbValue) ? this.props.chbColor : "grey", marginRight: "6px", color: this.props.chbValue == "Electric" ? "black" : "white" }}>{ this.props.chbValue }</button>
        );
    }
}