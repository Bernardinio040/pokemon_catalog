import React from "react";

export default class SearchBar extends React.Component {
    constructor(props){
        super(props);
    }

    searchFunction = (text) => {
        let searchedPokemons = this.props.pokedex.filter(pokemon => pokemon.name.english.toLowerCase().startsWith(text.toLowerCase()));
        this.props.dataUpdateCallback([...searchedPokemons]);
    }

    render(){
        return(
            <div className="searchBar">
                <h2>Search by name: </h2><input type="text" onChange={(x) => {this.searchFunction(x.target.value)}}></input>
            </div>
        );
    }
}