import React from "react";
import pokedex from './components/pokemon/assets/pokedex.json';
import types from './components/pokemon/assets/types.json';
import "./App.css";
import Tile from "./components/pokemon/tile";
import SearchBar from "./components/utils/search_bar";
import ToogleButton from "./components/utils/toogle_button";
import CheckboxFilterButton from "./components/utils/checkbox_filter_button";

export default class App extends React.Component {
  constructor(props){
    super(props);
    this.state = { pokedex : pokedex, sorting : "Sort by id asc", filters : [] };
    this.dataUpdate = this.dataUpdate.bind(this);
  }

  dataUpdate = (data, value, filter) => {
    this.setState({ pokedex : data });
    if (value) {
      this.setState({ sorting : value });
    }

    if (filter) {
      this.setState({ filters : filter });
    }
  }
  
  render(){
    let colors = ["#ccc9aa", "#e81319", "#5eb9b2", "#a819d7", "#e1d158", "#776a3e", "#bddd6e", "#8e55a4", "#7b8e8a", "#f67f0b", "#0a7abc", "#3e9709", "#fffa24", "#ec0e63", "#1995a1", "#8a55fd", "#5f4632", "#ffa0c2"];
    let i = 0;
    return (
      <div className="mainBox">
        <div className="searchBarBox">
          <SearchBar pokedex={ pokedex } dataUpdateCallback={ this.dataUpdate } />
        </div>
        <div className="sortingButtonsBox">
          <ToogleButton pokedex={ this.state.pokedex } bttValue={ "Sort by name asc" } bttType={{ by : "name", type : "asc" }} value={ this.state.sorting } dataUpdateCallback={ this.dataUpdate } />
          <ToogleButton pokedex={ this.state.pokedex } bttValue={ "Sort by name desc" } bttType={{ by : "name", type : "desc" }} value={ this.state.sorting } dataUpdateCallback={ this.dataUpdate } />
          <ToogleButton pokedex={ this.state.pokedex } bttValue={ "Sort by id asc" } bttType={{ by : "id", type : "asc" }} value={ this.state.sorting } dataUpdateCallback={ this.dataUpdate } />
          <ToogleButton pokedex={ this.state.pokedex } bttValue={ "Sort by id desc" } bttType={{ by : "id", type : "desc" }} value={ this.state.sorting } dataUpdateCallback={ this.dataUpdate } />
        </div>
        <div className="filterByTypeBox">
          {
              types.map((type)=>{
                return <CheckboxFilterButton key={ i } chbValue={ type.english } chbColor={ colors[i++] } filters = { this.state.filters } pokemons = { this.state.pokedex } dataUpdateCallback={ this.dataUpdate } />;
              })
          }
        </div>
        <div className="tilesBox">
          {
            this.state.pokedex.filter((x) => x.type.filter((y) =>  this.state.filters.includes(y)).length > 0 || this.state.filters.length == 0).map((pokemon)=>{
              return <Tile key={pokemon.id} id={pokemon.id} name={pokemon.name.english}/>;
            })
          }
        </div>
      </div>
    );
  }
}
